function addNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 + num2;
	return false;
}

function minNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 - num2;
	return false;
}

function timNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 * num2;
	return false;
}

function divNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 / num2;
	return false;
}

function modNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 % num2;
	return false;
}

function Clear(){
	location.reload(true);
}

document.getElementById('add3').addEventListener('click', addNumbers);
document.getElementById('sub4').addEventListener('click', minNumbers);
document.getElementById('mul1').addEventListener('click', timNumbers);
document.getElementById('div2').addEventListener('click', divNumbers);
document.getElementById('mod5').addEventListener('click', modNumbers);
document.getElementById('cle6').addEventListener('click', Clear);

